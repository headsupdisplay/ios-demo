# README #

To learn more or purchase a Heads Up Display, visit http://www.headsupdisplay.co

For additional support documentation:
https://drive.google.com/folderview?id=0B1bhATATxwIUWk02UGpZZmtyMHc

### What is this repository for? ###

This is a demonstration app for iOS that uses on-board sensors to trigger the Heads Up Display.

*It provides an examples of:
*How to negotiate a connection with the device using BlueRadios Drivers
*How to trigger the lights on heads up once a connection is established
*How to use different sensor readings to automatically trigger alerts on the device.

### Who do I talk to? ###

clark@headsupdisplay.co