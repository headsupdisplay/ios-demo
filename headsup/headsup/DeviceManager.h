//
//  DeviceManager.h
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Brsp.h"

@interface DeviceManager : NSObject

+ (void) SetActiveDevice:(Brsp*)b;
+ (Brsp*) getBrsp;

//0->9, A
+ (void) sendUpdate:(NSString*)val;

+ (void) stopHeartbeat;
+ (void) sendHeartbeat;


//FROM IPHONE
//- (void) getLoudness; //show DBs in the app and also update the device
//- (void) getPitch; //use accelerometer to update


//TODO: Emulation methods to tweak the output of loudness and pitch

@end
