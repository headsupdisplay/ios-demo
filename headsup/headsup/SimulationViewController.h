//
//  SimulationViewController.h
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimulationViewController : UIViewController

- (IBAction)demo:(id)sender;
@end
