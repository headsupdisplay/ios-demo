//
//  SoundViewController.h
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>


@interface SoundViewController : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *lblAmp;
@property (nonatomic, retain) IBOutlet UISlider *slider;

@property (nonatomic, retain) AVAudioRecorder *recorder;
@property (nonatomic, retain) NSTimer *levelTimer;

- (IBAction)zeroAmp:(id)sender;

@end
