//
//  HomeViewController.h
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Brsp.h"
#import "MBProgressHUD.h"

@interface HomeViewController : UIViewController <BrspDelegate, CBCentralManagerDelegate> {

    BrspMode _lastMode;
    MBProgressHUD *_hud;
}


@end
