//
//  PitchViewController.m
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import "PitchViewController.h"
#import "PerformSelectorWithDebounce.h"
#import "DeviceManager.h"

@interface PitchViewController ()
@property (nonatomic, assign) float xDegrees, yDegrees, zDegrees;
@end

@implementation PitchViewController

@synthesize slider;

@synthesize lblPitch;

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startMyMotionDetect];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.motionManager stopAccelerometerUpdates];
}


- (CMMotionManager *)motionManager
{
    CMMotionManager *motionManager = nil;
    
    id appDelegate = [UIApplication sharedApplication].delegate;
    
    if ([appDelegate respondsToSelector:@selector(motionManager)]) {
        motionManager = [appDelegate motionManager];
    }
    
    return motionManager;
}



- (void)startMyMotionDetect
{
    
    //    __block float stepMoveFactor = 15;
    
    [self.motionManager
     startAccelerometerUpdatesToQueue:[[NSOperationQueue alloc] init]
     withHandler:^(CMAccelerometerData *data, NSError *error)
     {
         
         dispatch_async(dispatch_get_main_queue(),
                        ^{
                            [self updateAccelerometerTo:data.acceleration.x :data.acceleration.y :data.acceleration.z];
                        }
                        );
     }
     ];
    
}

- (void) updateUI
{
    //    //    Left
    //    //    {y | -infinity <= y <=  -.35}
    //    //    Middle-Left
    //    //    {y | -.35  < y <  -.25}
    //    //    Centered
    //    //    {y | -.25  <= y <=  .25}
    //    //    Middle-Right
    //    //    {y | .26 < y < .35 }
    //    //    Right
    //    //    {y | .35 <= y <=  +infinity}
    
    NSString *cmd = @"0";
    
    float y = self.yDegrees + self.slider.value;
    
    if(y > -999999 && y <= -35)
    {
        self.lblPitch.text = @"Left";
        cmd = @"5";
    }
    else if(y > -35 && y < -25)
    {
        self.lblPitch.text = @"Middle-Left";
        cmd = @"6";
    }
    else if(y >= -25 && y <= 25)
    {
        self.lblPitch.text = @"Centered";
        cmd = @"7";
    }
    else if(y > -26 && y < 35)
    {
        self.lblPitch.text = @"Middle-Right";
        cmd = @"8";
    }
    else if(y >= -35 && y <= 999999)
    {
        self.lblPitch.text = @"Right";
        cmd = @"9";
    }
    NSLog(@"updateUI|%@|%f", cmd, y);
    
    [DeviceManager sendUpdate:cmd];
}

- (void) updateAccelerometerTo:(float)x :(float)y :(float)z
{
    self.xDegrees = RadiansToDegrees(x);
    self.yDegrees = RadiansToDegrees(y);
    self.zDegrees = RadiansToDegrees(z);
    
    [self performSelector:@selector(updateUI) withDebounceDuration:1.0f];
}


CGFloat DegreesToRadians(CGFloat degrees) {
    return degrees * M_PI / 180;
};

CGFloat RadiansToDegrees(CGFloat radians) {
    return radians * 180 / M_PI;
};

- (IBAction)zeroPitch:(id)sender
{
    self.slider.value = 0.0f;
}

@end


//TODO: Swipe to page


