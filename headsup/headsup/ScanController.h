//
//  ScanController.h
//  sampleterm
//
//  Created by Michael Testa on 11/1/12.
//  Copyright (c) Blueradios, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
//#import "ConnectionController.h"
#import "Brsp.h"

@interface ScanController : UIViewController <UITableViewDelegate, UITableViewDataSource, CBCentralManagerDelegate> {
    IBOutlet UITableView *_deviceTableView;
    IBOutlet UIBarButtonItem *_scanButton;
    NSMutableArray *_peripherals;
    BOOL _isScanning;
}

@property (strong, nonatomic) UITableView* deviceTableView;

//UI Elements
- (IBAction)scanButton:(id)sender;

- (IBAction)showVersionButton:(id)sender;
- (void)enableButton:(UIBarButtonItem*)butt;
- (void)disableButton:(UIBarButtonItem*)butt;
@end
