//
//  AppDelegate.h
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly) CMMotionManager *motionManager;

@property (strong, nonatomic) CBCentralManager *cbCentral;
@property (strong, nonatomic) CBPeripheral *activePeripheral;

//Returns a pointer to the shared AppDelegate
+(AppDelegate*)app;

@end
