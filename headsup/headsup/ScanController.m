//
//  ScanController.m
//  sampleterm
//
//  Created by Michael Testa on 11/1/12.
//  Copyright (c) Blueradios, Inc. All rights reserved.
//

#import "ScanController.h"

@implementation ScanController

@synthesize deviceTableView = _deviceTableView;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _peripherals = [NSMutableArray new];
    self.navigationItem.title = @"Select Device";
//    [self disableButton:_scanButton];
    [AppDelegate app].cbCentral = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [AppDelegate app].cbCentral.delegate = self;
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Start a scan
    [self startScan];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma mark - Table options

//**********************************************************************************************************************************************************
//Table Options 
//**********************************************************************************************************************************************************
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)TableView numberOfRowsInSection:(NSInteger)section {
    return _peripherals.count; 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	// Configure the cell.
    
    CBPeripheral *peripheral = [_peripherals objectAtIndex:indexPath.row];
    
    cell.textLabel.text = peripheral.name;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"cellSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"cellSegue"]) {
        [self stopScan];
        NSIndexPath *indexPath = [self.deviceTableView indexPathForSelectedRow];
        
        [AppDelegate app].activePeripheral = [_peripherals objectAtIndex:indexPath.row];      
        [self.deviceTableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {return NO;}

#pragma mark - UI 
- (IBAction)scanButton:(id)sender {
    if (!_isScanning) {
        [self startScan];
    } else {
        [self stopScan];
    }
}

-(void) startScan {
    _isScanning = YES;
    _scanButton.title = @"Scanning...";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [_peripherals removeAllObjects];
    [self.deviceTableView reloadData];
    [[AppDelegate app].cbCentral scanForPeripheralsWithServices:[NSArray arrayWithObject:[Brsp brspServiceUUID]] options:nil];
}

-(void) stopScan {
    _isScanning = NO;
    _scanButton.title = @"Scan";
    [[AppDelegate app].cbCentral stopScan];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (IBAction)showVersionButton:(id)sender {
    NSString *version =[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"];
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Version"
                          message: [NSString stringWithFormat:@"nBlueTerm version %@", version]
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)enableButton:(UIBarButtonItem*)butt {
    butt.enabled = YES;
}

- (void)disableButton:(UIBarButtonItem*)butt {
    butt.enabled = NO;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
}
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
}
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
}
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    if (![_peripherals containsObject:peripheral]) {
        [_peripherals addObject:peripheral];
        [self.deviceTableView reloadData];
    }
}
-(void)retrieveConnectedPeripherals {
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripheralslist {
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    printf("Status of CoreBluetooth central manager changed %d \r\n",central.state);
    if (central.state==CBCentralManagerStatePoweredOn) {
        [self enableButton:_scanButton];
    }
}
@end
