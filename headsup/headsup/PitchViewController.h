//
//  PitchViewController.h
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@interface PitchViewController : UIViewController

//motion test
- (CMMotionManager *)motionManager;

@property (nonatomic, retain) IBOutlet UILabel *lblPitch;

@property (nonatomic, retain) IBOutlet UISlider *slider;
- (IBAction)zeroPitch:(id)sender;
@end
