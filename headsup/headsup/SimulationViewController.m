//
//  SimulationViewController.m
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import "SimulationViewController.h"
#import "DeviceManager.h"

@interface SimulationViewController ()

@end

@implementation SimulationViewController

- (IBAction)demo:(id)sender
{
    int tag = ((UIButton*)sender).tag;
    
    NSString *cmd = [NSString stringWithFormat:@"%i", tag];
    if(tag == 11)
        cmd = @"A";
    
    [DeviceManager sendUpdate:cmd];
}

@end

//TODO: Swipe to page