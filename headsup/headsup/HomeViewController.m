//
//  HomeViewController.m
//  sampleterm
//
//  Created by Michael Testa on 11/1/12.
//  Copyright (c) Blueradios, Inc. All rights reserved.
//

#import "HomeViewController.h"
#import "DeviceManager.h"


//Make this number larger or smaller to see more or less output in the textview
#define MAX_TEXT_VIEW_CHARACTERS 1500
#define SAMPLE_APP_DEBUG_MODE 0  //Change to 1 to test bytes received with count and total time

@interface Brsp() {
    
}
@end

@implementation HomeViewController

//@synthesize brspObject;

- (Brsp*) brspObject
{
    return [DeviceManager getBrsp];
}

- (void) setBrspObject:(Brsp*)b
{
    [DeviceManager SetActiveDevice:b];
}

#pragma mark BrspDelegate
- (void)brsp:(Brsp*)brsp OpenStatusChanged:(BOOL)isOpen {
    NSLog(@"OpenStatusChanged == %d", isOpen);
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (isOpen) {
        //The BRSP object is ready to be used
//        [self enableButtons];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        //Print the security level of the brsp service to console
        NSLog(@"BRSP Security Level is %d", self.brspObject.securityLevel);
    } else {
        //brsp object has been closed
    }
}
- (void)brsp:(Brsp*)brsp SendingStatusChanged:(BOOL)isSending { //TODO: We probably only care if going into idle mode for this demo
//    //This is a good place to change BRSP mode
//    //If we are on the last command in the queue and we are no longer sending, change the mode back to previous value
//    if (isSending == NO && _commandQueue.count < 2)
//    {
//        if (_lastMode == self.brspObject.brspMode)
//            return;  //Nothing to do here
//        //Change mode back to previous setting
//        NSError *error = [self.brspObject changeBrspMode:_lastMode];
//        if (error)
//            NSLog(@"%@", error);
//    }
}

- (void)brsp:(Brsp*)brsp ErrorReceived:(NSError*)error {
    NSLog(@"%@", error.description);
}

#pragma mark CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    //call the open function to setup the brsp service
    _hud.labelText = @"Discovering characteristics";
    [self.brspObject open];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    [self.brspObject close];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [AppDelegate app].activePeripheral.name;
    _lastMode = BrspModeData; //Default brsp mode
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    [self disableButtons];
    [AppDelegate app].cbCentral.delegate = self;
    
    //init the object with default buffer sizes of 1024 bytes
    //self.brspObject = [[Brsp alloc] initWithPeripheral:[AppDelegate app].activePeripheral];
    
    //init with custom buffer sizes
    self.brspObject = [[Brsp alloc] initWithPeripheral:[AppDelegate app].activePeripheral InputBufferSize:512 OutputBufferSize:512];
    
    //It is important to set this delegate before calling [Brsp open]
    self.brspObject.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated {
    if(!_hud)
    {
        //Use CBCentral Manager to connect this peripheral
        _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hud.labelText = @"Connecting";
    }
    [[AppDelegate app].cbCentral connectPeripheral:[AppDelegate app].activePeripheral options:nil];
    
    [DeviceManager sendHeartbeat];
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [DeviceManager stopHeartbeat];
}

//TODO: Keep a connection for now
//-(void)viewDidDisappear:(BOOL)animated {
//    //call close to disable notifications etc (Not required)
//    [self.brspObject close];
//    //Use CBCentralManager to close the connection to this peripheral
//    [[AppDelegate app].cbCentral cancelPeripheralConnection:[AppDelegate app].activePeripheral];
//	[super viewWillDisappear:animated];
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    switch(interfaceOrientation)
    {
        case UIInterfaceOrientationLandscapeLeft:
            return NO;
        case UIInterfaceOrientationLandscapeRight:
            return NO;
        default:
            return YES;
    }
}

@end
