//
//  SoundViewController.m
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import "SoundViewController.h"
#import "PerformSelectorWithDebounce.h"
#import "DeviceManager.h"

@interface SoundViewController ()
@property (nonatomic, assign) float dbLevel;
@property (nonatomic, retain) NSString *lastCmd;
//@property (nonatomic, retain) MeterTable meterTable;
@end

@implementation SoundViewController

@synthesize slider;

@synthesize lblAmp;
@synthesize recorder, levelTimer;

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.lastCmd = @"";
    [self startSoundDetection];
}

- (void) viewDidLoad
{
    NSError *error;
    [[AVAudioSession sharedInstance]
     setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    
    if (error) {
        NSLog(@"Error setting category: %@", [error description]);
    }
    [super viewDidLoad];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self stopSoundDetection];
}

- (void) startSoundDetection
{

//    NSURL *url = [NSURL fileURLWithPath:@"/dev/null"];
    
//	NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
//							  [NSNumber numberWithFloat: 44100.0],                 AVSampleRateKey,
//							  [NSNumber numberWithInt: kAudioFormatAppleLossless], AVFormatIDKey,
//							  [NSNumber numberWithInt: 1],                         AVNumberOfChannelsKey,
//							  [NSNumber numberWithInt: AVAudioQualityMax],         AVEncoderAudioQualityKey,
//							  nil];
    
    NSDictionary* settings = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:kAudioFormatAppleIMA4],AVFormatIDKey,
                                      [NSNumber numberWithInt:44100],AVSampleRateKey,
                                      [NSNumber numberWithInt:1],AVNumberOfChannelsKey,
                                      [NSNumber numberWithInt:16],AVLinearPCMBitDepthKey,
                                      [NSNumber numberWithBool:NO],AVLinearPCMIsBigEndianKey,
                                      [NSNumber numberWithBool:NO],AVLinearPCMIsFloatKey,
                                      nil];
    
	NSError *error;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:nil];
    
    if(!self.recorder)
    {
//        self.recorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
        //http://b2cloud.com.au/tutorial/obtaining-decibels-from-the-ios-microphone/
        self.recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL URLWithString:[NSTemporaryDirectory() stringByAppendingPathComponent:@"tmp.caf"]]  settings:settings error:&error];
    }
    
    if (self.recorder) {
//        [self.recorder prepareToRecord];
        self.recorder.meteringEnabled = YES;
        [self.recorder record];
    } else
    {
        NSLog(@"%@", [error description]);
    }
    
    if(!levelTimer)
        levelTimer = [NSTimer scheduledTimerWithTimeInterval: 0.5f target: self selector: @selector(levelTimerCallback:) userInfo: nil repeats: YES];
}

- (void)levelTimerCallback:(NSTimer *)timer {
	[recorder updateMeters];
    
    //https://developer.apple.com/library/ios/documentation/AVFoundation/Reference/AVAudioRecorder_ClassReference/Reference/Reference.html#//apple_ref/occ/instm/AVAudioRecorder/averagePowerForChannel:
    //https://www.chem.purdue.edu/chemsafety/Training/PPETrain/dblevels.htm
    
    float db = [recorder averagePowerForChannel:0];
    db += 95;// 160
    db = db < 0 ? 0 : db;
    self.dbLevel = db;
    
    [self performSelector:@selector(updateUI) withDebounceDuration:1.0f];
}


- (void) stopSoundDetection
{
    [self.levelTimer invalidate];
    self.levelTimer = nil;
    
    [self.recorder stop];
}

- (IBAction)zeroAmp:(id)sender
{
    self.slider.value = 0.0f;
}

- (void) updateUI
{
    
//    Low
//    >0dB AND <= 85dB
//    Medium
//    >85dB AND <= 95dB
//    High
//    >95dB AND <= 105dB
//    Danger
//    >105dB

    NSString *cmd = @"0";
    BOOL forceShow = NO;
    
    float db = self.dbLevel + self.slider.value;
    
    if(db >= 0 && db <= 85)
    {
//        self.lblAmp.text = @"Low";
        cmd = @"1";
    }
    else if(db > 85 && db <= 95)
    {
//        self.lblAmp.text = @"Mediun";
        cmd = @"2";
    }
    else if(db > 95 && db <= 105)
    {
//        self.lblAmp.text = @"High";
        cmd = @"3";
    }
    else if(db > 105)
    {
//        self.lblAmp.text = @"Danger";
        cmd = @"4";
        forceShow = YES;
    }
    
    NSLog(@"updateUI|%@|%f", cmd, db);
    
    self.lblAmp.text = [NSString stringWithFormat:@"%.02f dB", db];
    
    if(forceShow || [self.lastCmd isEqualToString:cmd] == NO)
    {
        
        [DeviceManager sendUpdate:cmd];
    }
    self.lastCmd = cmd;
}

@end

//TODO: Swipe to page

