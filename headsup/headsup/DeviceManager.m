//
//  DeviceManager.m
//  headsup
//
//  Created by Michael Zorn on 2/12/14.
//  Copyright (c) 2014 Heads Up, Inc. All rights reserved.
//

#import "DeviceManager.h"
#import "PerformSelectorWithDebounce.h"

@implementation DeviceManager

static Brsp *brsp = nil;

static NSTimer *hbTimer = nil;

+ (void) SetActiveDevice:(Brsp*)b
{
    brsp = b;
}

+ (Brsp*) getBrsp
{
    return brsp;
}

+ (void) sendUpdate:(NSString *)val
{
    NSString *cmd = [NSString stringWithFormat:@"%@\r", val];
    
    NSLog(@"Send Update %@", cmd);
    
    NSError *error = [brsp writeString:cmd];
    if (error)
        NSLog(@"%@", error.description);

}

+ (void) stopHeartbeat
{
    if(hbTimer)
    {
        [hbTimer invalidate];
        hbTimer = nil;
    }
}

+ (void) sendHeartbeat
{
    
    if(!hbTimer)
    {
        hbTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                   target:self
                                                 selector:@selector(beat)
                                                 userInfo:nil
                                                  repeats:YES];
    }
}

+ (void) beat
{
    [self sendUpdate:@"0"];
}

@end
